#!/usr/bin/env python2

# Requires pycrypto. Install with:
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win-amd64-py2.7.exe
# if you're on win64 win python2.7 for 64bit or
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win32-py2.7.exe
# if you're using the 32bit environment

import sys, random, base64
from Crypto.Cipher import AES

def GenRandomData(Length):
    RandList = random.sample(xrange(256), Length)
    return ''.join(chr(Elem) for Elem in RandList)

AESKey = GenRandomData(16)

def PKCS7Pad(InData, BlockSize):
    PadLen = BlockSize - (len(InData) % BlockSize)
    PadByte = chr(PadLen)
    return InData + (PadByte * PadLen)

def AESECBEncrypt(InData, Key, BlockSize):
    Encrypted = ''
    Cypher = AES.new(Key, AES.MODE_ECB)
    for Pointer in xrange(0, len(InData), BlockSize):
        Block = InData[Pointer:Pointer + BlockSize]
        EncryptedBlock = Cypher.encrypt(Block)
        Encrypted += EncryptedBlock
    return Encrypted

def EncryptionOracle(InData):
    BlockSize = 16
    UnkString = ('Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg'
                'aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq'
                'dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg'
                'YnkK')
    UnkString = base64.b64decode(UnkString)
    InData = InData + UnkString
    PaddedData = PKCS7Pad(InData, BlockSize)
    Encrypted = AESECBEncrypt(PaddedData, AESKey, BlockSize)
    return Encrypted

def FindBlockSize():
    PadSize = 0
    StartSize = len(EncryptionOracle('A' * PadSize))
    EndSize = StartSize
    while (EndSize == StartSize):
        PadSize += 1
        String = 'A' * PadSize
        EndSize = len(EncryptionOracle(String))
    StartSize = len(EncryptionOracle('A' * PadSize))
    EndSize = StartSize
    BlockLen = 1
    while (EndSize == StartSize):
        BlockLen += 1
        String = 'A' * (BlockLen + PadSize)
        EndSize = len(EncryptionOracle(String))
    return BlockLen

def EncrDetectionOracle(BlockSize):
    UserData = GenRandomData(BlockSize) * 4
    Encrypted = EncryptionOracle(UserData)
    Block1 = Encrypted[BlockSize * 2:BlockSize * 3]
    Block2 = Encrypted[BlockSize * 3:BlockSize * 4]
    if (Block1 == Block2):
        return 0
    else:
        return 1

def BreakECB(BlockSize):
    ClearBlock = ''
    while True:
        ForgedLength = BlockSize - (len(ClearBlock) % BlockSize) - 1
        BaseString = 'A' * ForgedLength
        OneByteShortBlock = EncryptionOracle(BaseString)[0:len(BaseString) + len(ClearBlock) + 1]
        BrutedBlocks = []
        for BruteLetter in xrange(256):
            AppendByte = chr(BruteLetter)
            ForgedString = BaseString + ClearBlock + AppendByte
            BrutedBlocks.append(EncryptionOracle(ForgedString)[0:len(ForgedString)])
        if (OneByteShortBlock in BrutedBlocks):
            ClearBlock += chr(BrutedBlocks.index(OneByteShortBlock))
        else:
            return ClearBlock[:-1]

BlockSize = FindBlockSize()
print 'BlockSize: %d' % BlockSize

if (EncrDetectionOracle(BlockSize) == 0):
    print 'Encryption mode: ECB'
else:
    print 'Encryption mode: CBC'

OutFile = open(sys.argv[1], 'wb')
OutFile.write(BreakECB(BlockSize))
OutFile.close()
