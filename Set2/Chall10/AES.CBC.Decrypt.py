#!/usr/bin/env python2

# Requires pycrypto. Install with:
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win-amd64-py2.7.exe
# if you're on win64 win python2.7 for 64bit or
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win32-py2.7.exe
# if you're using the 32bit environment

import sys, base64
from Crypto.Cipher import AES

def XorStuffMultiByte(InData, XorKey):
    KeyCounter = 0
    CypherText = ''
    for TextChar in InData:
        CyphChar = chr(ord(TextChar) ^ ord(XorKey[KeyCounter % len(XorKey)]))
        CypherText += CyphChar
        KeyCounter += 1
    return CypherText

def AESCBCDecrypt(InData, Key, IV, BlockSize):
    Decrypted = ''
    Cypher = AES.new(Key, AES.MODE_ECB)
    for Pointer in xrange(0, len(InData), BlockSize):
        Block = InData[Pointer:Pointer + BlockSize]
        DecryptedBlock = Cypher.decrypt(Block)
        XORed = XorStuffMultiByte(DecryptedBlock, IV)
        IV = Block
        Decrypted += XORed
    PadLen = ord(Decrypted[len(Decrypted) - 1])
    Decrypted = Decrypted[:-PadLen]
    return Decrypted

BlockSize = 16
AESKey = 'YELLOW SUBMARINE'
AESIV = '\x00' * BlockSize

InFile = open(sys.argv[1], 'rb')
InData = InFile.read()
InFile.close()

Decoded = base64.b64decode(InData)
Decrypted = AESCBCDecrypt(Decoded, AESKey, AESIV, BlockSize)

OutFile = open(sys.argv[2], 'wb')
OutFile.write(Decrypted)
OutFile.close()
