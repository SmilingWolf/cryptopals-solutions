#!/usr/bin/env python2

# Requires pycrypto. Install with:
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win-amd64-py2.7.exe
# if you're on win64 win python2.7 for 64bit or
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win32-py2.7.exe
# if you're using the 32bit environment

import sys, base64
from Crypto.Cipher import AES

def PKCS7Pad(InData, BlockSize):
    PadLen = BlockSize - (len(InData) % BlockSize)
    PadByte = chr(PadLen)
    return InData + (PadByte * PadLen)

def XorStuffMultiByte(InData, XorKey):
    KeyCounter = 0
    CypherText = ''
    for TextChar in InData:
        CyphChar = chr(ord(TextChar) ^ ord(XorKey[KeyCounter % len(XorKey)]))
        CypherText += CyphChar
        KeyCounter += 1
    return CypherText

def AESCBCEncrypt(InData, Key, IV, BlockSize):
    Encrypted = ''
    InData = PKCS7Pad(InData, BlockSize)
    Cypher = AES.new(Key, AES.MODE_ECB)
    for Pointer in xrange(0, len(InData), BlockSize):
        Block = InData[Pointer:Pointer + BlockSize]
        XORed = XorStuffMultiByte(Block, IV)
        EncryptedBlock = Cypher.encrypt(XORed)
        IV = EncryptedBlock
        Encrypted += EncryptedBlock
    return Encrypted

def AESCBCDecrypt(InData, Key, IV, BlockSize):
    Decrypted = ''
    Cypher = AES.new(Key, AES.MODE_ECB)
    for Pointer in xrange(0, len(InData), BlockSize):
        Block = InData[Pointer:Pointer + BlockSize]
        DecryptedBlock = Cypher.decrypt(Block)
        XORed = XorStuffMultiByte(DecryptedBlock, IV)
        IV = Block
        Decrypted += XORed
    PadLen = ord(Decrypted[len(Decrypted) - 1])
    Decrypted = Decrypted[:-PadLen]
    return Decrypted

BlockSize = 16
AESKey = 'YELLOW SUBMARINE'
AESIV = '\x00' * BlockSize

SelfTestData = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdef'
Encrypted = AESCBCEncrypt(SelfTestData, AESKey, AESIV, BlockSize)
assert (Encrypted == '\xF5\x45\xC0\x06\x06\x91\x26\xD9\xC0\xF9\x3F\xA7\xDD\x89\xAB\x98\xEE\x93\x9F\x5D\xE3\x28\xC0\x5F\x4A\x9D\x64\x79\xD3\x88\x2A\x39\x42\xC5\xA1\x8F\xC3\xCC\xA9\x52\x4F\xAF\x41\x06\xC9\x89\x23\xC1')
Decrypted = AESCBCDecrypt(Encrypted, AESKey, AESIV, BlockSize)
assert (Decrypted == SelfTestData)
print 'It works!'
