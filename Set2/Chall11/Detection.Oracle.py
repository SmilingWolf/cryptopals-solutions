#!/usr/bin/env python2

# Requires pycrypto. Install with:
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win-amd64-py2.7.exe
# if you're on win64 win python2.7 for 64bit or
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win32-py2.7.exe
# if you're using the 32bit environment

# So what are we doing here? Well, to hell if I know.
# Let's just say it's black magic, mkay?
# Jokes apart, in EncrDetectionOracle I'm creating
# 4 equal blocks of random data. Those emulate user input.
# I'm creating this many because there is garbage both at the
# beginning and at the end of the data (as required by the
# exercise's description), plus padding data.
# So I discard the first encrypted block and take the
# second and the third. If they are equal ECB was used, since
# that's its feature/vulnerability.
# That is, same plaintext block == same encrypted block.
# If they do not match CBC was used.

import sys, random
from Crypto.Cipher import AES

def GenRandomData(Length):
    RandList = random.sample(xrange(256), Length)
    return ''.join(chr(Elem) for Elem in RandList)

def PKCS7Pad(InData, BlockSize):
    PadLen = BlockSize - (len(InData) % BlockSize)
    PadByte = chr(PadLen)
    return InData + (PadByte * PadLen)

def AESECBEncrypt(InData, Key, BlockSize):
    Encrypted = ''
    Cypher = AES.new(Key, AES.MODE_ECB)
    InData = PKCS7Pad(InData, BlockSize)
    for Pointer in xrange(0, len(InData), BlockSize):
        Block = InData[Pointer:Pointer + BlockSize]
        EncryptedBlock = Cypher.encrypt(Block)
        Encrypted += EncryptedBlock
    return Encrypted

def XorStuffMultiByte(InData, XorKey):
    KeyCounter = 0
    CypherText = ''
    for TextChar in InData:
        CyphChar = chr(ord(TextChar) ^ ord(XorKey[KeyCounter % len(XorKey)]))
        CypherText += CyphChar
        KeyCounter += 1
    return CypherText

def AESCBCEncrypt(InData, Key, IV, BlockSize):
    Encrypted = ''
    InData = PKCS7Pad(InData, BlockSize)
    Cypher = AES.new(Key, AES.MODE_ECB)
    for Pointer in xrange(0, len(InData), BlockSize):
        Block = InData[Pointer:Pointer + BlockSize]
        XORed = XorStuffMultiByte(Block, IV)
        EncryptedBlock = Cypher.encrypt(XORed)
        IV = EncryptedBlock
        Encrypted += EncryptedBlock
    return Encrypted

def EncryptionOracle(InData, BlockSize):
    PrependLen = random.randint(5, 10)
    AppendLen = random.randint(5, 10)
    PrependBytes = GenRandomData(PrependLen)
    AppendBytes = GenRandomData(AppendLen)
    InData = PrependBytes + InData + AppendBytes
    AESKey = GenRandomData(BlockSize)
    AESIV = GenRandomData(BlockSize)
    if random.randint(0, 1) == 0:
        print 'Going ECB'
        Encrypted = AESECBEncrypt(InData, AESKey, BlockSize)
    else:
        print 'Going CBC'
        Encrypted = AESCBCEncrypt(InData, AESKey, AESIV, BlockSize)
    return Encrypted

def EncrDetectionOracle(BlockSize):
    UserData = GenRandomData(BlockSize) * 4
    Encrypted = EncryptionOracle(UserData, BlockSize)
    Block1 = Encrypted[BlockSize * 2:BlockSize * 3]
    Block2 = Encrypted[BlockSize * 3:BlockSize * 4]
    if (Block1 == Block2):
        return 0
    else:
        return 1

BlockSize = 16
if (EncrDetectionOracle(BlockSize) == 0):
    print 'Encryption mode: ECB'
else:
    print 'Encryption mode: CBC'
