#!/usr/bin/env python2

import sys

def PKCS7Pad(InData, BlockSize):
    PadLen = BlockSize - (len(InData) % BlockSize)
    PadByte = chr(PadLen)
    return InData + (PadByte * PadLen)

assert (PKCS7Pad('YELLOW SUBMARINE', 16) == 'YELLOW SUBMARINE\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10')
assert (PKCS7Pad('YELLOW SUBMARINE', 20) == 'YELLOW SUBMARINE\x04\x04\x04\x04')
print 'It works!'
