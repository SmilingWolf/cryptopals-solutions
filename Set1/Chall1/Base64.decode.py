#!/usr/bin/var python2

import sys

def Base64Decode(InData):
    DecodedBytes = ''
    Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    for Elem in xrange(0, len(InData), 4):
        Piece1 = Alphabet.index(InData[Elem]) << 2
        Piece2 = Alphabet.index(InData[Elem + 1]) >> 4
        DecodedBytes += chr(Piece1 | Piece2)
        if (InData[Elem + 2] == '='):
            break
        Piece1 = (Alphabet.index(InData[Elem + 1]) & 0x0F) << 4
        Piece2 = Alphabet.index(InData[Elem + 2]) >> 2
        DecodedBytes += chr(Piece1 | Piece2)
        if (InData[Elem + 3] == '='):
            break
        Piece1 = (Alphabet.index(InData[Elem + 2]) & 0x03) << 6
        Piece2 = Alphabet.index(InData[Elem + 3])
        DecodedBytes += chr(Piece1 | Piece2)
    return DecodedBytes

def HexEncode(InData):
    EncodedBytes = ''
    for Elem in InData:
        High = ord(Elem) >> 4
        Low = ord(Elem) & 0x0F
        if (High >= 0x00 and High <= 0x09):
            High += 0x30
        elif (High >= 0x0A and High <= 0x0F):
            High += 0x57
        if (Low >= 0x00 and Low <= 0x09):
            Low += 0x30
        elif (Low >= 0x0A and Low <= 0x0F):
            Low += 0x57
        EncodedBytes += chr(High) + chr(Low)
    return EncodedBytes

InFile = open(sys.argv[1], 'rb')
InData = InFile.read()
InFile.close()

Decoded = Base64Decode(InData)
Encoded = HexEncode(Decoded)

OutFile = open(sys.argv[2], 'wb')
OutFile.write(Encoded)
OutFile.close()
