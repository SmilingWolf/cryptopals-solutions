#!/usr/bin/var python2

import sys

def HexDecode(InData):
    DecodedBytes = ''
    for Elem in xrange(0, len(InData), 2):
        High = ord(InData[Elem])
        Low = ord(InData[Elem + 1])
        if (High >= 0x30 and High <= 0x39):
            High = (High - 0x30) << 4
        elif (High >= 0x61 and High <= 0x66):
            High = (High - 0x57) << 4
        if (Low >= 0x30 and Low <= 0x39):
            Low = Low - 0x30
        elif (Low >= 0x61 and Low <= 0x66):
            Low = Low - 0x57
        DecodedBytes += chr(High | Low)
    return DecodedBytes

def Base64Encode(InData):
    Piece1 = 0
    Piece2 = 0
    EncodedBytes = ''
    Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    for Elem in xrange(0, len(InData), 3):
        Piece1 = ord(InData[Elem]) >> 2
        Piece2 = (ord(InData[Elem]) & 0x03) << 4
        EncodedBytes += Alphabet[Piece1]
        if (len(InData) - Elem == 1):
            EncodedBytes += Alphabet[Piece2]
            EncodedBytes += '=='
            break
        else:
            Piece1 = Piece2 | (ord(InData[Elem + 1]) >> 4)
            Piece2 = (ord(InData[Elem + 1]) & 0x0F) << 2
            EncodedBytes += Alphabet[Piece1]
        if (len(InData) - Elem == 2):
            EncodedBytes += Alphabet[Piece2]
            EncodedBytes += '='
        else:
            Piece1 = Piece2 | (ord(InData[Elem + 2]) >> 6)
            Piece2 = ord(InData[Elem + 2]) & 0x3F
            EncodedBytes += Alphabet[Piece1]
            EncodedBytes += Alphabet[Piece2]
    return EncodedBytes

InFile = open(sys.argv[1], 'rb')
InData = InFile.read()
InFile.close()

Decoded = HexDecode(InData)
Encoded = Base64Encode(Decoded)

OutFile = open(sys.argv[2], 'wb')
OutFile.write(Encoded)
OutFile.close()
