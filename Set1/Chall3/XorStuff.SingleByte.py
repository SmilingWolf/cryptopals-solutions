#!/usr/bin/env python

import sys

xorkey = int(sys.argv[2], 16)
cyphertext = []

infile = open(sys.argv[1], 'rb')
text = infile.read()
infile.close()

for textchar in text:
    cyphchar = chr(ord(textchar) ^ xorkey)
    cyphertext.append(cyphchar)

outfile = open('cyph.bin', 'wb')
outfile.write(b''.join(cyphertext))
outfile.close()
