#!/usr/bin/env python

import sys

lst = [0] * 256

infile = open(sys.argv[1], 'rb')
text = infile.read()
infile.close()

for textchar in text:
    textchar = ord(textchar)
    if (textchar >= 0x41 and textchar <= 0x5A):
        textchar += 0x20
    if (textchar == 0x20 or (textchar >= 0x61 and textchar <= 0x7A)):
        lst[textchar] += 1

#print lst

freqdict = {}
for elem in xrange(len(lst)):
    if lst[elem] != 0:
        freqdict[chr(elem)] = lst[elem]

freqsorted = sorted(freqdict.items(), key = lambda field: field[1], reverse = True)

PrioList = []
for elem in xrange(len(freqsorted)):
    PrioList.append(freqsorted[elem][0])

PrioList = ''.join(PrioList)
print 'PrioList = \'%s\'' % PrioList
