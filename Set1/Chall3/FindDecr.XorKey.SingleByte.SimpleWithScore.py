#!/usr/bin/env python

# Now I'm trying to score the various cleartexts (all 256 of them) basing the decision on
# the matching of the characters frequency with the generated one (PrioList)
# so that then I can pick the one with the highest score and check the key.
# The scoring system is as follows: each letter in the frequency list has got an assigned value
# based on its position in the PrioList: len(Priolist) for the first char, len - 1 for the 2nd
# and so on. This is because the first letters are less likely to change position when building
# the PrioList from a random english text than the last. I'm sort of assigning a weight to the
# statistical significance of every letter. Thing is, I'm ONLY checking how many of the
# characters from the first list appear in the EXACT SAME position in the second.
# It works "well enough" on large enough texts, but I think I should be assigning a lowered
# score to characters appearing out of order based based on how much they are distant
# from their position. That's why I still defined this implementation Simple.

import sys

# Characters ordered by frequency based on generic english texts
PrioList = ' etaoinshrdlcumwfgypbvkjxqz'

def TextFreqCount(data):
    lst = [0] * 256

    for textchar in data:
        textchar = ord(textchar)
        if (textchar >= 0x41 and textchar <= 0x5A):
            textchar += 0x20
        if (textchar == 0x20 or (textchar >= 0x61 and textchar <= 0x7A)):
            lst[textchar] += 1

    freqdict = {}
    for elem in xrange(len(lst)):
        if lst[elem] != 0:
            freqdict[chr(elem)] = lst[elem]

    freqsorted = sorted(freqdict.items(), key = lambda field: field[1], reverse = True)
    return freqsorted

def XorStuff(data, xorkey):
    cyphertext = []

    for textchar in data:
        cyphchar = chr(ord(textchar) ^ xorkey)
        cyphertext.append(cyphchar)
    return b''.join(cyphertext)

def AssignScore(listone, listtwo):
    score = 0
    for elem in xrange(len(listone)):
        if (listone[elem] == listtwo[elem]):
            score += len(listtwo) - elem
    return score

infile = open(sys.argv[1], 'rb')
text = infile.read()
infile.close()

KeyScoreDict = {}
for key in xrange(0, 256):
    cleartext = XorStuff(text, key)
    TCurPriList = TextFreqCount(cleartext)
    CurPrioList = []
    for elem in xrange(len(TCurPriList)):
        CurPrioList.append(TCurPriList[elem][0])
    CurPrioList = ''.join(CurPrioList)
    Score = AssignScore(CurPrioList, PrioList)
    KeyScoreDict[key] = Score

ScoreSorted = sorted(KeyScoreDict.items(), key = lambda field: field[1], reverse = True)
print 'The key is: 0x%02X' % ScoreSorted[0][0]

outfile = open('clear.bin', 'wb')
outfile.write(XorStuff(text, ScoreSorted[0][0]))
outfile.close()
