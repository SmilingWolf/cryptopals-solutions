#!/usr/bin/env python

# Simplest approach: use the most frequent character from the cyphertext and xor it against
# the statistically most frequent char, give in output the "decrypted" text.
# There is still no cleartext scoring here, so this is kinda lame. I'm still getting used to
# python and its datatypes, so this is more a demo where I try out stuff.

import sys

# Characters ordered by frequency based on generic english texts
PrioList = ' etaoinshrdlcumwfgypbvkjxqz'

def BinFreqCount(data):
    lst = [0] * 256

    for textchar in data:
        textchar = ord(textchar)
        lst[textchar] += 1

    freqdict = {}
    for elem in xrange(len(lst)):
        if lst[elem] != 0:
            freqdict[chr(elem)] = lst[elem]

    freqsorted = sorted(freqdict.items(), key = lambda field: field[1], reverse = True)
    return freqsorted

def XorStuff(data, xorkey):
    cyphertext = []

    for textchar in data:
        cyphchar = chr(ord(textchar) ^ xorkey)
        cyphertext.append(cyphchar)
    return b''.join(cyphertext)

infile = open(sys.argv[1], 'rb')
text = infile.read()
infile.close()

CharFreq = BinFreqCount(text)
DecrKey = ord(CharFreq[0][0]) ^ ord(PrioList[0])
print 'Decryption Key: 0x%02X' % DecrKey

outfile = open('clear.bin', 'wb')
outfile.write(XorStuff(text, DecrKey))
outfile.close()
