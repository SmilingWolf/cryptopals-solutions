#!/usr/bin/env python

# Classify the keysizes between 2 and 40 based on the Hamming
# distance of two keysize long blocks of cyphertext.
# The lowest normalized Hamming distances (that is, HammDist/keyzise)
# define the most likely keysizes.
# Calculate the Hamming distances of N consecutive blocks of cyphertext,
# then output the average of the normalized distances.
# This grants us an higher degree of precision.
# Thanks Mr. eXoDia for a discussion that brought this to
# converge much faster

import sys

BlocksNum = int(sys.argv[2])

def CalcHammDist(str1, str2):
    HammDist = 0

    if (len(str1) == len(str2)):
        for elem in xrange(len(str1)):
            diff = ord(str1[elem]) ^ ord(str2[elem])
            for shift in xrange(8):
                if ((diff >> shift) & 1):
                    HammDist = HammDist + 1
    else:
        HammDist = False
    return HammDist

infile = open(sys.argv[1], 'rb')
text = infile.read()
infile.close()

HammDict = {}
for keysize in xrange(2, 41):
    HammDist = 0
    Intervallo1 = 0
    DistancesList = []
    BlocksList = []
    for block in xrange(BlocksNum):
        Intervallo2 = Intervallo1 + keysize
        slice = text[Intervallo1:Intervallo2]
        BlocksList.append(slice)
        Intervallo1 = Intervallo2
    for x in xrange(len(BlocksList)):
        for y in xrange(x + 1, len(BlocksList)):
            Result = CalcHammDist(BlocksList[x], BlocksList[y]) / float(keysize)
            DistancesList.append(Result)
    if (False not in DistancesList):
        AverageDist = sum(DistancesList) / len(DistancesList)
        HammDict[keysize] = AverageDist

HammSorted = sorted(HammDict.items(), key = lambda field: field[1])
for elem in xrange(len(HammSorted)):
    print '%d: %f' % (HammSorted[elem][0], HammSorted[elem][1])
