#!/usr/bin/env python

# This one must be one of the most epic things I have ever coded.
# It's almost like fucking magic!
# Anyway, down with the technical part.
# First of all calculate the most likely keysize. I limited the possibilities
# between 2 and 40.
# This script takes in input the file with the cyphertext, chooses the
# keysize, divides the cyphertext in blocks of keysize length and transposes
# them (taking the first byte of every block and putting it in a new block
# and so on). Then it uses the already devised single byte xor decryption key
# finding algo on every transposed block, joins the retrieved keys and done!
# You've got the Vigenere key!

import sys

def TextFreqCount(data):
    lst = [0] * 256

    for textchar in data:
        textchar = ord(textchar)
        if (textchar >= 0x41 and textchar <= 0x5A):
            textchar += 0x20
        if (textchar == 0x20 or (textchar >= 0x61 and textchar <= 0x7A)):
            lst[textchar] += 1

    freqdict = {}
    for elem in xrange(len(lst)):
        if lst[elem] != 0:
            freqdict[chr(elem)] = lst[elem]

    freqsorted = sorted(freqdict.items(), key = lambda field: field[1], reverse = True)
    return freqsorted

def XorStuffSingleByte(data, xorkey):
    cyphertext = []
    for textchar in data:
        cyphchar = chr(ord(textchar) ^ xorkey)
        cyphertext.append(cyphchar)
    return b''.join(cyphertext)

def XorStuffMultiByte(data, xorkey):
    keycounter = 0
    cyphertext = []
    for textchar in data:
        cyphchar = chr(ord(textchar) ^ ord(xorkey[keycounter % len(xorkey)]))
        cyphertext.append(cyphchar)
        keycounter += 1
    return b''.join(cyphertext)

def AssignScore(listone, listtwo):
    score = 0
    for elem in xrange(len(listone)):
        if (listone[elem] == listtwo[elem]):
            score += len(listtwo) - elem
        else:
            charpos = listtwo.find(listone[elem])
            lessened = abs(elem - charpos)
            score += len(listtwo) - elem - lessened
    return score

def FindKey(cypherblock):
    KeyScoreDict = {}
    PrioList = ' etaoinshrdlcumwfgypbvkjxqz'
    for key in xrange(0, 256):
        cleartext = XorStuffSingleByte(cypherblock, key)
        TCurPriList = TextFreqCount(cleartext)
        CurPrioList = []
        for elem in xrange(len(TCurPriList)):
            CurPrioList.append(TCurPriList[elem][0])
        CurPrioList = ''.join(CurPrioList)
        Score = AssignScore(CurPrioList, PrioList)
        KeyScoreDict[key] = Score
    ScoreSorted = sorted(KeyScoreDict.items(), key = lambda field: field[1], reverse = True)
    return ScoreSorted[0][0]

def CalcHammDist(str1, str2):
    HammDist = 0

    if (len(str1) == len(str2)):
        for elem in xrange(len(str1)):
            diff = ord(str1[elem]) ^ ord(str2[elem])
            for shift in xrange(8):
                if ((diff >> shift) & 1):
                    HammDist = HammDist + 1
    else:
        HammDist = False
    return HammDist

def ChooseKeySize(data, minrange, maxrange, BlocksNum):
    HammDict = {}
    for keysize in xrange(minrange, maxrange):
        HammDist = 0
        Intervallo1 = 0
        DistancesList = []
        BlocksList = []
        for block in xrange(BlocksNum):
            Intervallo2 = Intervallo1 + keysize
            slice = data[Intervallo1:Intervallo2]
            BlocksList.append(slice)
            Intervallo1 = Intervallo2
        for x in xrange(len(BlocksList)):
            for y in xrange(x + 1, len(BlocksList)):
                Result = CalcHammDist(BlocksList[x], BlocksList[y]) / float(keysize)
                DistancesList.append(Result)
        if (False not in DistancesList):
            AverageDist = sum(DistancesList) / len(DistancesList)
            HammDict[keysize] = AverageDist

    HammSorted = sorted(HammDict.items(), key = lambda field: field[1])
    return HammSorted[0][0]

infile = open(sys.argv[1], 'rb')
text = infile.read()
infile.close()

# 4 blocks are usually enough to make the function
# converge on the right keysize, so that's the default
if (len(sys.argv) == 3):
    BlocksToInspect = int(sys.argv[2])
else:
    BlocksToInspect = 4

KeySize = ChooseKeySize(text, 2, 41, BlocksToInspect)

BlockList = []
for elem in xrange(KeySize):
    BlockList.append(text[elem::KeySize])

LikelyKeysList = []
for elem in xrange(len(BlockList)):
    LikelyKeysList.append(chr(FindKey(BlockList[elem])))

VigenereKey = ''.join(LikelyKeysList)
print 'Key: %s' % VigenereKey

outfile = open('clear.bin', 'wb')
outfile.write(XorStuffMultiByte(text, VigenereKey))
outfile.close()
