#!/usr/bin/env python

# Classify the keysizes between 2 and 40 based on the Hamming
# distance of two keysize long blocks of cyphertext.
# The lowest normalized Hamming distances (that is, HammDist/keyzise)
# define the most likely keysizes.
# Output the Hamming distance of only two consecutive blocks.

import sys

def CalcHammDist(str1, str2):
    HammDist = 0

    for elem in xrange(len(str1)):
        diff = ord(str1[elem]) ^ ord(str2[elem])
        for shift in xrange(8):
            if ((diff >> shift) & 1):
                HammDist = HammDist + 1
    return HammDist

infile = open(sys.argv[1], 'rb')
text = infile.read()
infile.close()

HammDict = {}
for keysize in xrange(2, 40):
    slice1 = text[0:keysize]
    slice2 = text[keysize:keysize*2]
    HammDist = CalcHammDist(slice1, slice2) / float(keysize)
    HammDict[keysize] = HammDist

HammSorted = sorted(HammDict.items(), key = lambda field: field[1])
print HammSorted
