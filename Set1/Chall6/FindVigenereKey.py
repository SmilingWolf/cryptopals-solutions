#!/usr/bin/env python

import sys

PrioList = ' etaoinshrdlcumwfgypbvkjxqz'

def TextFreqCount(data):
    lst = [0] * 256

    for textchar in data:
        textchar = ord(textchar)
        if (textchar >= 0x41 and textchar <= 0x5A):
            textchar += 0x20
        if (textchar == 0x20 or (textchar >= 0x61 and textchar <= 0x7A)):
            lst[textchar] += 1

    freqdict = {}
    for elem in xrange(len(lst)):
        if lst[elem] != 0:
            freqdict[chr(elem)] = lst[elem]

    freqsorted = sorted(freqdict.items(), key = lambda field: field[1], reverse = True)
    return freqsorted

def XorStuff(data, xorkey):
    cyphertext = []

    for textchar in data:
        cyphchar = chr(ord(textchar) ^ xorkey)
        cyphertext.append(cyphchar)
    return b''.join(cyphertext)

def AssignScore(listone, listtwo):
    score = 0
    for elem in xrange(len(listone)):
        if (listone[elem] == listtwo[elem]):
            score += len(listtwo) - elem
        else:
            charpos = listtwo.find(listone[elem])
            lessened = abs(elem - charpos)
            score += len(listtwo) - elem - lessened
    return score

def FindKey(cypherblock):
    KeyScoreDict = {}
    for key in xrange(0, 256):
        cleartext = XorStuff(cypherblock, key)
        TCurPriList = TextFreqCount(cleartext)
        CurPrioList = []
        for elem in xrange(len(TCurPriList)):
            CurPrioList.append(TCurPriList[elem][0])
        CurPrioList = ''.join(CurPrioList)
        Score = AssignScore(CurPrioList, PrioList)
        KeyScoreDict[key] = Score
    ScoreSorted = sorted(KeyScoreDict.items(), key = lambda field: field[1], reverse = True)
    return ScoreSorted[0][0]

infile = open(sys.argv[1], 'rb')
text = infile.read()
infile.close()

KeySize = int(sys.argv[2])

BlockList = []
for elem in xrange(KeySize):
    BlockList.append(text[elem::KeySize])

LikelyKeysList = []
for elem in xrange(len(BlockList)):
    LikelyKeysList.append(chr(FindKey(BlockList[elem])))

print 'Key:', ''.join(LikelyKeysList)
