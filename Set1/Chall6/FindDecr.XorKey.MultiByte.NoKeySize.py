#!/usr/bin/env python

# This one must be one of the most epic things I have ever coded.
# It's almost like fucking magic!
# Anyway, down with the technical part.
# First of all calculate the most likely keysize. I limited the possibilities
# between 2 and 40. Since this is an error prone step I have decided to leave
# it in a standalone script (actually 3, the ChooseKeySize scripts).
# See their descriptions to check what's different between them.
# This script takes in input the file with the cyphertext and the chosen
# keysize, divides the cyphertext in blocks of keysize length and transposes
# them (taking the first byte of every block and putting it in a new block
# and so on). Then it uses the already devised single byte xor decryption key
# finding algo on every transposed block, joins the retrieved keys and done!
# You've got the Vigenere key!

import sys

def TextFreqCount(data):
    lst = [0] * 256

    for textchar in data:
        textchar = ord(textchar)
        if (textchar >= 0x41 and textchar <= 0x5A):
            textchar += 0x20
        if (textchar == 0x20 or (textchar >= 0x61 and textchar <= 0x7A)):
            lst[textchar] += 1

    freqdict = {}
    for elem in xrange(len(lst)):
        if lst[elem] != 0:
            freqdict[chr(elem)] = lst[elem]

    freqsorted = sorted(freqdict.items(), key = lambda field: field[1], reverse = True)
    return freqsorted

def XorStuff(data, xorkey):
    cyphertext = []
    for textchar in data:
        cyphchar = chr(ord(textchar) ^ xorkey)
        cyphertext.append(cyphchar)
    return b''.join(cyphertext)
    
def XorStuffMultiByte(data, xorkey):
    keycounter = 0
    cyphertext = []
    for textchar in data:
        cyphchar = chr(ord(textchar) ^ ord(xorkey[keycounter % len(xorkey)]))
        cyphertext.append(cyphchar)
        keycounter += 1
    return b''.join(cyphertext)

def AssignScore(listone, listtwo):
    score = 0
    for elem in xrange(len(listone)):
        if (listone[elem] == listtwo[elem]):
            score += len(listtwo) - elem
        else:
            charpos = listtwo.find(listone[elem])
            lessened = abs(elem - charpos)
            score += len(listtwo) - elem - lessened
    return score

def FindKey(cypherblock):
    KeyScoreDict = {}
    PrioList = ' etaoinshrdlcumwfgypbvkjxqz'
    for key in xrange(0, 256):
        cleartext = XorStuff(cypherblock, key)
        TCurPriList = TextFreqCount(cleartext)
        CurPrioList = []
        for elem in xrange(len(TCurPriList)):
            CurPrioList.append(TCurPriList[elem][0])
        CurPrioList = ''.join(CurPrioList)
        Score = AssignScore(CurPrioList, PrioList)
        KeyScoreDict[key] = Score
    ScoreSorted = sorted(KeyScoreDict.items(), key = lambda field: field[1], reverse = True)
    return ScoreSorted[0][0]

infile = open(sys.argv[1], 'rb')
text = infile.read()
infile.close()

KeySize = int(sys.argv[2])

BlockList = []
for elem in xrange(KeySize):
    BlockList.append(text[elem::KeySize])

LikelyKeysList = []
for elem in xrange(len(BlockList)):
    LikelyKeysList.append(chr(FindKey(BlockList[elem])))

VigenereKey = ''.join(LikelyKeysList)
print 'Key: %s' % VigenereKey

outfile = open('clear.bin', 'wb')
outfile.write(XorStuffMultiByte(text, VigenereKey))
outfile.close()
