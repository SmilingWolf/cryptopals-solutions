#!/usr/bin/env python

str1 = 'this is a test'
str2 = 'wokka wokka!!!'

HammDist = 0

for elem in xrange(len(str1)):
    diff = ord(str1[elem]) ^ ord(str2[elem])
    for shift in xrange(8):
        if ((diff >> shift) & 1):
            HammDist = HammDist + 1

print HammDist
