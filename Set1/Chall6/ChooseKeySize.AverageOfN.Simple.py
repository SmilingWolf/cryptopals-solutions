#!/usr/bin/env python

# Classify the keysizes between 2 and 40 based on the Hamming
# distance of two keysize long blocks of cyphertext.
# The lowest normalized Hamming distances (that is, HammDist/keyzise)
# define the most likely keysizes.
# Calculate the Hamming distances of N consecutive blocks of cyphertext,
# then output the average of the normalized distances.
# This grants us an higher degree of precision.

import sys

BlocksNum = int(sys.argv[2]) - 1

def CalcHammDist(str1, str2):
    HammDist = 0

    if (len(str1) == len(str2)):
        for elem in xrange(len(str1)):
            diff = ord(str1[elem]) ^ ord(str2[elem])
            for shift in xrange(8):
                if ((diff >> shift) & 1):
                    HammDist = HammDist + 1
    else:
        HammDist = False
    return HammDist

infile = open(sys.argv[1], 'rb')
text = infile.read()
infile.close()

HammDict = {}
for keysize in xrange(2, 41):
    HammDist = 0
    Intervallo1 = 0
    for block in xrange(BlocksNum):
        Intervallo2 = Intervallo1 + keysize
        Intervallo3 = Intervallo2
        Intervallo4 = Intervallo3 + keysize
        slice1 = text[Intervallo1:Intervallo2]
        slice2 = text[Intervallo3:Intervallo4]
        Result = CalcHammDist(slice1, slice2) / float(keysize)
        if (Result != False):
            HammDist += Result
        else:
            HammDist = False
            break
        Intervallo1 = Intervallo3
    if (HammDist != False):
        HammDict[keysize] = HammDist / BlocksNum

HammSorted = sorted(HammDict.items(), key = lambda field: field[1])
print HammSorted
