#!/usr/bin/env python

import sys

xorkey = sys.argv[2]
cyphertext = []

infile = open(sys.argv[1], 'rb')
text = infile.read()
infile.close()

keycounter = 0
for textchar in text:
    cyphchar = chr(ord(textchar) ^ ord(xorkey[keycounter % len(xorkey)]))
    cyphertext.append(cyphchar)
    keycounter += 1

outfile = open('cyph.bin', 'wb')
outfile.write(b''.join(cyphertext))
outfile.close()
