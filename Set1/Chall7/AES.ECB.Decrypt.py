#!/usr/bin/env python2

# Requires pycrypto. Install with:
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win-amd64-py2.7.exe
# if you're on win64 win python2.7 for 64bit or
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win32-py2.7.exe
# if you're using the 32bit environment

import sys, base64
from Crypto.Cipher import AES

AESKey = 'YELLOW SUBMARINE'

InFile = open(sys.argv[1], 'rb')
InData = InFile.read()
InFile.close()

Decoded = base64.b64decode(InData)

Cypher = AES.new(AESKey, AES.MODE_ECB)
Decrypted = Cypher.decrypt(Decoded)

OutFile = open(sys.argv[2], 'wb')
OutFile.write(Decrypted)
OutFile.close()
