#!/usr/bin/env python

# This one must be one of the most epic things I have ever coded.
# It's almost like fucking magic!
# Anyway, down with the technical part.
# First of all calculate the most likely keysize. I limited the possibilities
# between 2 and 40.
# This script takes in input the file with the cyphertext, chooses the
# keysize, divides the cyphertext in blocks of keysize length and transposes
# them (taking the first byte of every block and putting it in a new block
# and so on). Then it uses magic on every transposed block,
# joins the retrieved keys and done! You've got the Vigenere key!
# N.B.: the Hamming distance is NOT a good test on Vigenere cyphertexts where
# only a small subset of the possible bytes has been encrypted (e.g. here we
# are talking only about the lowercase letters of the english alphabet).
# It just happens to work. With enough data. Sometimes. Maybe. If you're lucky.
# You get the drift.

import sys

def TextFreqCount(data):
    lst = [0] * 256

    for textchar in data:
        textchar = ord(textchar)
        if (textchar >= 0x61 and textchar <= 0x7A):
            lst[textchar] += 1

    freqdict = {}
    for elem in xrange(len(lst)):
        if lst[elem] != 0:
            freqdict[chr(elem)] = lst[elem]

    freqsorted = sorted(freqdict.items(), key = lambda field: field[1], reverse = True)
    return freqsorted

def CaesarShift(Text, Alphabet, NumPos):
    TranspAlpha = Alphabet[NumPos:len(Alphabet)] + Alphabet[:NumPos]
    NewText = ''
    for Elem in Text:
        if Elem in Alphabet:
            NewText += TranspAlpha[Alphabet.index(Elem)]
        else:
            NewText += Elem
    return NewText

def VigenereDecrypt(Text, VigKey):
    Alphabet = 'abcdefghijklmnopqrstuvwxyz'
    VigKey = VigKey.lower()
    KeySize = len(VigKey)
    TranspMatrix = []
    NewText = ''
    for Elem in xrange(KeySize):
        TranspMatrix.append(Text[Elem::KeySize])
    for Elem in xrange(len(TranspMatrix)):
        NumPos = len(Alphabet) - Alphabet.index(VigKey[Elem % KeySize])
        NewBlock = CaesarShift(TranspMatrix[Elem], Alphabet, NumPos)
        TranspMatrix[Elem] = NewBlock
    for Letter in xrange(len(TranspMatrix[0])):
        for Elem in xrange(len(TranspMatrix)):
            if (Letter < len(TranspMatrix[Elem])):
                NewText += TranspMatrix[Elem][Letter]
    return NewText

def XorStuffMultiByte(data, xorkey):
    keycounter = 0
    cyphertext = []
    for textchar in data:
        cyphchar = chr(ord(textchar) ^ ord(xorkey[keycounter % len(xorkey)]))
        cyphertext.append(cyphchar)
        keycounter += 1
    return b''.join(cyphertext)

def AssignScore(listone, listtwo):
    score = 0
    for elem in xrange(len(listone)):
        if (listone[elem] == listtwo[elem]):
            score += len(listtwo) - elem
        else:
            charpos = listtwo.find(listone[elem])
            lessened = abs(elem - charpos)
            score += len(listtwo) - elem - lessened
    return score

def FindKey(CypherBlock):
    KeyScoreDict = {}
    PrioList = 'etaoinshrdlcumwfgypbvkjxqz'
    Alphabet = 'abcdefghijklmnopqrstuvwxyz'
    for key in xrange(len(Alphabet)):
        NumPos = len(Alphabet) - key
        cleartext = CaesarShift(CypherBlock, Alphabet, NumPos)
        TCurPriList = TextFreqCount(cleartext)
        CurPrioList = []
        for elem in xrange(len(TCurPriList)):
            CurPrioList.append(TCurPriList[elem][0])
        CurPrioList = ''.join(CurPrioList)
        Score = AssignScore(CurPrioList, PrioList)
        KeyScoreDict[Alphabet[key]] = Score
    ScoreSorted = sorted(KeyScoreDict.items(), key = lambda field: field[1], reverse = True)
    return ScoreSorted[0][0]

def CalcHammDist(str1, str2):
    HammDist = 0

    if (len(str1) == len(str2)):
        for elem in xrange(len(str1)):
            diff = ord(str1[elem]) ^ ord(str2[elem])
            for shift in xrange(8):
                if ((diff >> shift) & 1):
                    HammDist = HammDist + 1
    else:
        HammDist = False
    return HammDist

def ChooseKeySize(data, minrange, maxrange, BlocksNum):
    HammDict = {}
    for keysize in xrange(minrange, maxrange):
        HammDist = 0
        Intervallo1 = 0
        DistancesList = []
        BlocksList = []
        for block in xrange(BlocksNum):
            Intervallo2 = Intervallo1 + keysize
            slice = data[Intervallo1:Intervallo2]
            BlocksList.append(slice)
            Intervallo1 = Intervallo2
        for x in xrange(len(BlocksList)):
            for y in xrange(x + 1, len(BlocksList)):
                Result = CalcHammDist(BlocksList[x], BlocksList[y]) / float(keysize)
                DistancesList.append(Result)
        if (False not in DistancesList):
            AverageDist = sum(DistancesList) / len(DistancesList)
            HammDict[keysize] = AverageDist

    HammSorted = sorted(HammDict.items(), key = lambda field: field[1])
    return HammSorted[0][0]

infile = open(sys.argv[1], 'rb')
text = infile.read()
infile.close()

# 4 blocks are usually enough to make the function
# converge on the right keysize, so that's the default
if (len(sys.argv) == 3):
    BlocksToInspect = int(sys.argv[2])
else:
    BlocksToInspect = 4

KeySize = ChooseKeySize(text, 2, 41, BlocksToInspect)

BlockList = []
for elem in xrange(KeySize):
    BlockList.append(text[elem::KeySize])

LikelyKeysList = []
for elem in xrange(len(BlockList)):
    LikelyKeysList.append(FindKey(BlockList[elem]))

VigenereKey = ''.join(LikelyKeysList)
print 'Key: %s' % VigenereKey

outfile = open('clear.bin', 'wb')
outfile.write(VigenereDecrypt(text, VigenereKey))
outfile.close()
