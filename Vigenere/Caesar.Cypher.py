#!/usr/bin/env python

import sys

rotkey = int(sys.argv[2])

infile = open(sys.argv[1], 'rb')
text = infile.read()
infile.close()

def caesarshift(text, number):
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    transpalha = alphabet[number:len(alphabet)] + alphabet[:number]
    newtext = ''
    for elem in text:
        if elem in alphabet:
            newtext += transpalha[alphabet.index(elem)]
        else:
            newtext += elem
    return newtext

cyphertext = caesarshift(text, rotkey)

outfile = open('cyph.bin', 'wb')
outfile.write(cyphertext)
outfile.close()
