#!/usr/bin/env python

# We could optimize this thing a little bit by precalculating
# all of the transposed alphabets in VigenereCrypt so that
# CaesarShift doesn't have to repeat the same operation
# over and over. I'm just leaving this as a TODO.

import sys

VigKey = sys.argv[2]

InFile = open(sys.argv[1], 'rb')
Text = InFile.read()
InFile.close()

def CaesarShift(Text, Alphabet, NumPos):
    TranspAlpha = Alphabet[NumPos:len(Alphabet)] + Alphabet[:NumPos]
    NewText = ''
    for Elem in Text:
        if Elem in Alphabet:
            NewText += TranspAlpha[Alphabet.index(Elem)]
        else:
            NewText += Elem
    return NewText

def VigenereDecrypt(Text, VigKey):
    Alphabet = 'abcdefghijklmnopqrstuvwxyz'
    VigKey = VigKey.lower()
    KeySize = len(VigKey)
    TranspMatrix = []
    NewText = ''
    for Elem in xrange(KeySize):
        TranspMatrix.append(Text[Elem::KeySize])
    for Elem in xrange(len(TranspMatrix)):
        NumPos = len(Alphabet) - Alphabet.index(VigKey[Elem % KeySize])
        NewBlock = CaesarShift(TranspMatrix[Elem], Alphabet, NumPos)
        TranspMatrix[Elem] = NewBlock
    for Letter in xrange(len(TranspMatrix[0])):
        for Elem in xrange(len(TranspMatrix)):
            if (Letter < len(TranspMatrix[Elem])):
                NewText += TranspMatrix[Elem][Letter]
    return NewText

ClearText = VigenereDecrypt(Text, VigKey)

outfile = open('clear.bin', 'wb')
outfile.write(ClearText)
outfile.close()
